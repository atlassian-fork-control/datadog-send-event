# Bitbucket Pipelines Pipe: Datadog Send Event

Sends an event to [Datadog](https://datadoghq.com). It allows you to programatically post events to the [event stream](https://docs.datadoghq.com/graphing/event_stream/) and visualize and search for them in the UI.
                          
## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/datadog-send-event:1.1.1
  variables:
    API_KEY: '<string>'
    TITLE: '<string>'
    # TEXT: '<string>' # Optional.
    # PRIORITY: '<string>' # Optional.
    # ALERT_TYPE: '<string>' # Optional.
    # TAGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
    # EU_ENDPOINT: '<boolean>' # Optional.
```

## Variables

| Variable          | Usage                                                       |
| ------------------| ----------------------------------------------------------- |
| API_KEY (*)       | The name that will be printed in the logs |
| TITLE (*)         | The event title. Limited to 100 characters. |
| TEXT              | The body of the event. Limited to 4000 characters. The text supports markdown. Default: `Alert sent from Pipeline #${BITBUCKET_BUILD_NUMBER}` which links to the build.|
| PRIORITY          | The priority of the event: normal or low. Default: `normal`. Valid values are: `normal` and `low`. |
| ALERT_TYPE        | The alert type. Default: `info`. Valid values are: `error`, `warning`, `info`, and `success`. |
| TAGS              | A list of tags to apply to the event. Default: `'"branch:$BITBUCKET_BRANCH", "workspace:$BITBUCKET_WORKSPACE", "repository:$BITBUCKET_REPO_SLUG"'`|
| DEBUG             | Turn on extra debug information. Default: `false`. |
| EU_ENDPOINT       | If true then use `"https://api.datadoghq.eu/api/"` endpoint, else - `"https://api.datadoghq.com/api/"`. Default: `false`. |


_(*) = required variable._


## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/datadog-send-event:1.1.1
    variables:
      API_KEY: $API_KEY
      TITLE: 'This is the title'
```

Advanced example:

```yaml
script:
  - pipe: atlassian/datadog-send-event:1.1.1
    variables:
      API_KEY: $API_KEY
      TITLE: 'This is the title'
      TEXT: 'This is the description'
      PRIORITY: 'low'
      ALERT_TYPE: 'error'
      TAGS: '"mytag1:foo", "mytag2:bar"'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,datadog
