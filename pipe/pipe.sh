#!/usr/bin/env bash
#
# Sends an event to Datadog, https://docs.datadoghq.com/api/?lang=bash#events
#
# Required globals:
#   API_KEY
#   TITLE
#
# Optional globals:
#   TEXT
#   PRIORITY
#   ALERT_TYPE
#   TAGS
#   DEBUG
#   EU_ENDPOINT

source "$(dirname "$0")/common.sh"

extra_args=""
if [[ "${DEBUG}" == "true" ]]; then
  extra_args="--verbose"
fi

# mandatory variables
API_KEY=${API_KEY:?'API_KEY variable missing.'}
TITLE=${TITLE:?'TITLE variable missing.'}

# default variables
TEXT=${TEXT:="%%% \n Alert sent from [Pipeline #${BITBUCKET_BUILD_NUMBER}](https://bitbucket.org/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}) \n %%%"}
PRIORITY=${PRIORITY:='normal'}
ALERT_TYPE=${ALERT_TYPE:='info'}
TAGS=\[${TAGS:="\"branch:$BITBUCKET_BRANCH\", \"workspace:$BITBUCKET_WORKSPACE\", \"repository:$BITBUCKET_REPO_SLUG\""}\]
EU_ENDPOINT=${EU_ENDPOINT:=false}

curl_output_file="/tmp/pipe-$RANDOM.txt"

payload=$(jq -n \
  --arg TITLE "${TITLE}" \
  --arg TEXT "${TEXT}" \
  --arg PRIORITY "${PRIORITY}" \
  --arg ALERT_TYPE "${ALERT_TYPE}" \
  --argjson TAGS "${TAGS}" \
'{
    "title": $TITLE,
    "text": $TEXT,
    "priority": $PRIORITY,
    "alert_type": $ALERT_TYPE,
    "source_type_name": "BITBUCKET",
    "tags": $TAGS
}')

endpoint=`$EU_ENDPOINT && echo "app.datadoghq.eu" || echo "app.datadoghq.com"`

run curl -s -X POST -H "Content-type: application/json" --output ${curl_output_file} -w "%{http_code}" \
    -d "$payload" \
    ${extra_args} \
    "https://$endpoint/api/v1/events?api_key=$API_KEY"

response=$(cat ${curl_output_file})
info "HTTP Response: $(cat $response)"

if [[ "$(cat ${output_file})" = 2* ]]; then
  url=$(echo ${response} | jq -r '.event.url')
  success "Datadog event sent successfully. You can check your event here: $url"
else
  fail "Something failed. Datadog event was not sent."
fi
